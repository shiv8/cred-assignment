//
//  ViewController.swift
//  CredAssignment
//
//  Created by Apple on 03/03/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var paymentBtn: UIButton!
    
    @IBOutlet weak var credView: CredView!
    
    var hideFlag:Int = 0
    var direction: Direction = .down
    var currentState:StackViewState = .collapsed
    
    @IBAction func paymentBtnAction(_ sender: Any) {
        if currentState == .collapsed {
            currentState = .expand
            credView.setupView(currentState: .expand)
            return
        }
        currentState = .collapsed
        credView.setupView(currentState: .collapsed)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

