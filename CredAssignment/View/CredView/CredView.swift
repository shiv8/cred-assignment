//
//  CredView.swift
//  CredAssignment
//
//  Created by Apple on 03/03/21.
//

import Foundation
import UIKit

enum HideType {
    case first
    case second
    case third
}
enum Direction {
    case up
    case down
}

class CredView: UIView {
    
    @IBOutlet weak var paymentStackView: UIStackView!
    @IBOutlet weak var paymentFixView: UIView!
    @IBOutlet weak var paymentCollapsableView: UIView!
    @IBOutlet weak var heightForPaymentView: NSLayoutConstraint!
    
    
    
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
        let topView = Bundle.main.loadNibNamed("CredView", owner: self, options: nil)?.first as! UIView
        self.addSubview(topView)
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        topView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        topView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        topView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
    
    
    func setupView(currentState:StackViewState) {
        if currentState == .collapsed {
            paymentFixView.isHidden = false
            paymentCollapsableView.isHidden = true
            UIView.animate(withDuration: 0.25) {
                //self.paymentStackView.removeConstraint(self.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0))
                self.heightForPaymentView.constant = 60
                self.layoutIfNeeded()
                self.superview?.layoutIfNeeded()
            }
            return
            
        }
        paymentFixView.isHidden = true
        paymentCollapsableView.isHidden = false
        UIView.animate(withDuration: 0.25) {
            self.heightForPaymentView.constant = 600
            self.layoutIfNeeded()
            self.superview?.layoutIfNeeded()
        }
        return
    }
    
    
}


enum StackViewState {
    case collapsed
    case expand
}
