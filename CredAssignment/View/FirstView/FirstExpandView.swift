//
//  FirstExpandView.swift
//  CredAssignment
//
//  Created by Apple on 03/03/21.
//

import Foundation
import UIKit

class FirstExpandView: UIView {
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
        let topView = Bundle.main.loadNibNamed("FirstExpandView", owner: self, options: nil)?.first as! UIView
        self.addSubview(topView)
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        topView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        topView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        topView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
}
